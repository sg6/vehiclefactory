/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiclefactory;

/**
 *
 * @author stefan
 */
class Factory{
	public static final int CAR = 0;
	public static final int MOTORCYCLE = 1;
	
	public Vehicle createVehicle(int i){
		switch(i){
		case CAR:
			return new Car();
		case MOTORCYCLE:
			return new Motorcycle();
		default:
			throw new IllegalArgumentException(
				"Wrong vehicle number!");
		}
	}
}
