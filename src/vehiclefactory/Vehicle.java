/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiclefactory;

/**
 *
 * @author stefan
 */
public interface Vehicle {
    public int getCountOfTires();
    public String getName();
}
