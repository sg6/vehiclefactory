/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiclefactory;

import java.util.ArrayList;

/**
 *
 * @author stefan
 */
public class VehicleFactory {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Factory f = new Factory();

        Vehicle newCar = f.createVehicle(Factory.CAR);
        Vehicle newMotorcycle = f.createVehicle(Factory.MOTORCYCLE);

        System.out.println(
                newCar.getName() + ": " + 
                newCar.getCountOfTires() + 
                " tires");
        System.out.println(newMotorcycle.getName()
                + ": " + newMotorcycle.getCountOfTires()
                + " tires");
    }
    
}
